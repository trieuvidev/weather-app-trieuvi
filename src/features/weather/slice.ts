import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { get, isEmpty } from "lodash-es";
import weatherApis from "../../apis/weather";
import { RootState } from "../../app/store";
import { DEFAULT_QUESTION } from "../../constants/question";
import { IWeather } from "../../types/weather";

interface IInitialState {
  currentWeather: IWeather | null;
  question: string
  loading: boolean;
  error: string | null;
}

const initialState: IInitialState = {
  currentWeather: null,
  loading: false,
  error: null,
  question: DEFAULT_QUESTION
};

export const fetchWeatherData = createAsyncThunk(
  "weather/fetchWeatherData",
  async (zipcode: string, { rejectWithValue }) => {
    try {
      const response = await weatherApis.detail({ query: zipcode });
      return response.data;
    } catch (error) {
      return rejectWithValue(
        get(error, "response.data", "Somethings went wrong")
      );
    }
  }
);

const weatherSlice = createSlice({
  name: "weather",
  initialState,
  reducers: {
    onSelectQuestion: (state, {payload}: PayloadAction<string>) => {
      state.question = payload
      if(!isEmpty(state.currentWeather)){
        state.currentWeather = null
      }
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchWeatherData.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(fetchWeatherData.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload.success === false) {
          state.error = action.payload?.error?.info;
        } else {
          state.currentWeather = action.payload.current;
        }
      })
      .addCase(fetchWeatherData.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      });
  },
});
export const weatherSelector = (state: RootState) => state.weather;
export const weatherActions = weatherSlice.actions;
export const weatherReducer = weatherSlice.reducer;
export default weatherSlice.reducer;
