export interface IQuestionView  {
    id: string,
    question: string
}