export interface IWeather {
  cloudcover: number;
  feelslike: number;
  humidity: number;
  is_day: string;
  observation_time: string;
  precip: number;
  pressure: number;
  temperature: number;
  uv_index: number;
  visibility: number;
  weather_code: number;
  weather_icons: string[];
  wind_degree: number;
  wind_dir: string;
  wind_speed: number;
}
