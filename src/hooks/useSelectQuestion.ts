import { useAppDispatch, useAppSelector } from "../app/hooks";
import { weatherActions, weatherSelector } from "../features/weather/slice";

const useSelectQuestion = () => {
  const dispatch = useAppDispatch()
  const {question} = useAppSelector(weatherSelector)

  const onSelectHandler = (value: string) => dispatch(weatherActions.onSelectQuestion(value))

  return {onSelectHandler, question};
};

export default useSelectQuestion;
