import { useDispatch } from "react-redux";
import { AppDispatch } from "../app/store";
import { fetchWeatherData } from "../features/weather/slice";

const useFetchWeather = () => {
  const dispatch: AppDispatch = useDispatch();
  const onFetchCurrentWeather = (zipcode: string) => {
    dispatch(fetchWeatherData(zipcode));
  };
  return { onFetchCurrentWeather };
};

export default useFetchWeather;
