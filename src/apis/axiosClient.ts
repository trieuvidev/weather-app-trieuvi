import axios from "axios";
import { ACCESS_KEY, API_BASE_URL } from "../constants/api";
const axiosClient = axios.create({
  baseURL: API_BASE_URL,
  headers: {
    "Content-Type": "application/json",
  },
  params: {
    access_key: ACCESS_KEY,
  },
});

export default axiosClient;
