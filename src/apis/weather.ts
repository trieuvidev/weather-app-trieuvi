import axiosClient from "./axiosClient";

const weatherApis = {
  detail(params: Record<string, string>): Promise<any> {
    const url = `/current`;
    return axiosClient.get(url, { params });
  },
};

export default weatherApis;
