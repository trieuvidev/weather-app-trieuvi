import { Flex, RadioGroup, Stack } from "@chakra-ui/react";
import { QUESTIONS } from "../../constants/question";
import useSelectQuestion from "../../hooks/useSelectQuestion";
import Question from "../Question";

const Questions = () => {
  const { onSelectHandler, question } = useSelectQuestion();
  return (
    <Flex>
      <RadioGroup value={question} onChange={onSelectHandler}>
        <Stack spacing={8} direction="column">
          {QUESTIONS.map((ques) => (
            <Question key={ques.id} question={ques} />
          ))}
        </Stack>
      </RadioGroup>
    </Flex>
  );
};

export default Questions;
