import { Alert, AlertIcon, Flex } from "@chakra-ui/react";
import { useAppSelector } from "../../app/hooks";
import { DEFAULT_QUESTION, QUESTION_2, QUESTION_3 } from "../../constants/question";
import { NO, RAINING_NO, RAINING_YES, YES } from "../../constants/weather";
import { weatherSelector } from "../../features/weather/slice";

const Message = () => {
  const { currentWeather, question } = useAppSelector(weatherSelector);

  const renderMessage = () => {
    if (currentWeather) {
      switch (question) {
        case DEFAULT_QUESTION:
          if (currentWeather.precip === 0) {
            return RAINING_YES;
          } else return RAINING_NO;
          case QUESTION_2:
          if (currentWeather.uv_index > 3) {
            return YES;
          } else return NO;
          case QUESTION_3:
            if (currentWeather.precip === 0 && currentWeather.wind_speed > 15) {
              return YES;
            } else return NO;
        default:
          break;
      }
    }
  };

  return (
    <Flex>
      <Alert status="success">
        <AlertIcon />
        {renderMessage()}
      </Alert>
    </Flex>
  );
};

export default Message;
