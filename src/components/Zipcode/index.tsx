import { Button, Flex, Input, Text, useToast } from "@chakra-ui/react";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { useAppSelector } from "../../app/hooks";
import { weatherSelector } from "../../features/weather/slice";
import useFetchWeather from "../../hooks/useFetchWeather";

const schema = yup.object().shape({
  zipcode: yup.string().required("Zipcode is required"),
});

function Zipcode() {
  const { onFetchCurrentWeather } = useFetchWeather();
  const { error } = useAppSelector(weatherSelector);
  const toast = useToast();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = ({ zipcode = "" }) => {
    onFetchCurrentWeather(zipcode);
  };

  if (error !== null) {
    toast({ title: error , status: 'error'});
  }

  return (
    <Flex>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Flex flexDirection="column" gap="20px">
          <Flex alignContent="flex-start" gap="10px" flexDirection="column">
            <Input {...register("zipcode")} placeholder="Enter zipcode..." />
            {errors.zipcode && (
              <Text color="red" textAlign="left">
                {errors?.zipcode?.message as string}
              </Text>
            )}
          </Flex>
          <Button type="submit" display="flex">
            Submit
          </Button>
        </Flex>
      </form>
    </Flex>
  );
}

export default Zipcode;
