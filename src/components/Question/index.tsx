import { Radio, Text } from "@chakra-ui/react";
import { FC } from "react";
import { IQuestionView } from "../../types/question";

interface IQuestion {
  question: IQuestionView;
}

const Question: FC<IQuestion> = ({ question }) => {
  return (
    <Radio value={question.id}>
      <Text opacity={1} fontSize="16px" fontWeight={500} color="black">
        {question.question}
      </Text>
    </Radio>
  );
};

export default Question;
