import { extendTheme } from "@chakra-ui/react";
const breakpoints = {
  sm: "320px",
  md: "768px",
  lg: "960px",
  xl: "1200px",
};
export const theme = extendTheme({
  colors: {},
  styles: {
    global: {
      "*": {
        lineHeight: 1.15,
        fontSize: "16px",
        "&::-webkit-scrollbar": {
          display: "none",
        },
        "&::-webkit-scrollbar-track": {
          background: "#E1E1E1",
        },
        "&::-webkit-scrollbar-thumb": {
          background: "#E1E1E1",
          height: "10px",
          borderRadius: "24px",
        },
        overflowScrolling: "touch",
        touchAction: "pan-y",
        scrollBehavior: "smooth",
      },
      div: {
        "&::-webkit-scrollbar": {
          width: "5px",
        },
        "&::-webkit-scrollbar-track": {
          width: "5px",
        },
        "&::-webkit-scrollbar-thumb": {
          background: "#E1E1E1",
          borderRadius: "24px",
        },
      },
    },
  },
  components: {
    Checkbox: {
      baseStyle: {
        control: {
          width: "20px",
          height: "20px",
          bg: "white",
          borderColor: "#B0B3BC",
          borderRadius: "4px",
          _checked: {
            bg: "#0E19FC",
            borderColor: "#0E19FC",
            width: "20px",
            height: "20px",
            borderRadius: "4px",
          },
          _hover: {
            _checked: {
              bg: "#0E19FC",
              borderColor: "#0E19FC",
            },
          },
        },
      },
    },
  },
  breakpoints,
});
