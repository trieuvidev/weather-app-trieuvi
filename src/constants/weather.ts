export const RAINING_YES = "Yes if it’s not raining";
export const RAINING_NO = "No if it’s raining";
export const YES = "Yes";
export const NO = "No";
