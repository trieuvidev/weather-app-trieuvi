export const DEFAULT_QUESTION = 'question1'
export const QUESTION_2 = 'question2'
export const QUESTION_3 = 'question3'

export const QUESTIONS = [
  {
    id: DEFAULT_QUESTION,
    question: "Should I go outside?",
  },
  {
    id: QUESTION_2,
    question: "Should I wear sunscreen?",
  },
  {
    id: QUESTION_3,
    question: "Can I fly my kite?",
  },
];


