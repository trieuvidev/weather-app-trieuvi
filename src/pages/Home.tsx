import { Flex } from "@chakra-ui/react";
import { isEmpty } from "lodash-es";
import { useAppSelector } from "../app/hooks";
import Message from "../components/Message";
import Questions from "../components/Questions";
import Zipcode from "../components/Zipcode";
import { weatherSelector } from "../features/weather/slice";

const Home = () => {
  const { currentWeather } = useAppSelector(weatherSelector);
  return (
    <Flex gap="40px" flexDirection="column" padding="40px">
      <Questions />
      <Zipcode />
      {!isEmpty(currentWeather) ? <Message /> : null}
    </Flex>
  );
};

export default Home;
